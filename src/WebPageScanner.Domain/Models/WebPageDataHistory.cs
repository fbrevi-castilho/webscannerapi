﻿using System;
using System.Collections.Generic;

namespace WebPageScanner.Domain.Models
{
    public class WebPageDataHistory
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public List<string> Images { get; set; }
        public List<Word> Words { get; set; }
        public DateTime Created { get; set; }
        
    }
}
