﻿namespace WebPageScanner.Domain.Models
{
    public class Word
    {
        public string Value { get; set; }
        public int Ocurrences { get; set; }
    }
}
