﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using WebPageScanner.Domain.Interfaces.Repository;
using WebPageScanner.Domain.Interfaces.Service;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Domain.Services
{
    public class ScannerService : IScannerService
    {
        private readonly IScannerRepository _scannerRepository;

        public ScannerService(IScannerRepository scannerRepository)
        {
            _scannerRepository = scannerRepository;
        }


        public WebPageDataHistory GetWebPageData(string url)
        {
            var webPageDataHistory =  GetPage(url);
            _scannerRepository.InsertOne(webPageDataHistory);
            return webPageDataHistory;
        }
       

        private WebPageDataHistory GetPage(string url)
        {
            WebPageDataHistory webPageDataHistory = new WebPageDataHistory();

            WebClient client = new WebClient();
            using (Stream data = client.OpenRead(url))
            {
                using (StreamReader reader = new StreamReader(data))
                {
                   string content = reader.ReadToEnd();
                    webPageDataHistory.Images = GetSrcImages(content);
                    webPageDataHistory.Words = GetAndCountWords(content);
                    webPageDataHistory.Url = url;
                    webPageDataHistory.Created = DateTime.UtcNow;                    
                }
            }
            
            return webPageDataHistory;
        }

        private List<string> GetSrcImages(string content)
        {
            List<string> imgs = new List<string>();
            var pageDoc = new HtmlDocument();
            pageDoc.LoadHtml(content);
            imgs.AddRange(pageDoc.DocumentNode.Descendants("img")
                                            .Select(e => e.GetAttributeValue("src", null))
                                            .Where(s => !String.IsNullOrEmpty(s) && (s.Contains(".jpg") || s.Contains(".png"))));
            return imgs.Distinct().ToList();
        }

        private List<Word> GetAndCountWords(string content)
        {                    
            var pageDoc = new HtmlDocument();
            pageDoc.LoadHtml(content);          

            var root = pageDoc.DocumentNode;
            root = RemoveStylesAndComments(root);
            var onlyText = GetHtmlText(root);
            var words = SplitTextToWordList(onlyText);

            //only words with 3+ letters
            return words.Where(x=> x.Value.Length > 3).OrderByDescending(x => x.Ocurrences).Take(10).ToList();
        }

        private HtmlNode RemoveStylesAndComments(HtmlNode node)
        {
            //remove styles
            node.Descendants()
                .Where(n => n.Name == "script" || n.Name == "style")
                .ToList()
                .ForEach(n => n.Remove());

            //remove comments
            node.Descendants()
                 .Where(n => n.NodeType == HtmlNodeType.Comment)
                 .ToList()
                 .ForEach(n => n.Remove());

            return node;
        }

        private string GetHtmlText(HtmlNode node)
        {
            var sb = new StringBuilder();
            foreach (var n in node.DescendantsAndSelf())
            {
                if (!n.HasChildNodes)
                {
                    string text = n.InnerHtml;
                    if (!string.IsNullOrEmpty(text))
                        sb.AppendLine(text.Trim());
                }
            }
            //remove remaining HTML tags
            var txt = Regex.Replace(sb.ToString(), "<.*?>", string.Empty);
            //remove numbers
            txt = Regex.Replace(txt, @"[\d-]", string.Empty);

            return txt;
        }

        private List<Word> SplitTextToWordList(string text)
        {            
            char[] delimiters = new char[] { ' ', '\r', '\t', '\n', '.', ',', '?', '!', ':', '|', '/', '\\' };
            var words = text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).ToList();
            
            var group = words.GroupBy(x => x.ToLower());
            
            List<Word> wrds = new List<Word>();
            foreach (var grp in group)
            {
                wrds.Add(new Word { Value = grp.Key, Ocurrences = grp.Count() });
            }

            return wrds;
        }
    }
}
