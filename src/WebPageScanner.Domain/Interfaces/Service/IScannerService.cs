﻿using WebPageScanner.Domain.Models;

namespace WebPageScanner.Domain.Interfaces.Service
{
    public interface IScannerService
    {
        public WebPageDataHistory GetWebPageData(string url);
    }
}
