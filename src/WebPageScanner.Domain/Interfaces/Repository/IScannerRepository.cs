﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Domain.Interfaces.Repository
{
    public interface IScannerRepository
    {
        List<WebPageDataHistory> GetPageData(string url, int minutes);
        void InsertOne(WebPageDataHistory webPageDataHistory);
    }
}
