﻿namespace WebPageScanner.Domain.Configurations
{
    public class DbSettingsOptions
    {
        public string MongoConnection { get; set; }
        public string DataBase { get; set; }
    }
}
