﻿using System.Text.RegularExpressions;

namespace WebPageScanner.Domain.Validations
{
    public static class ValidationScannerController
    {
        public static bool isValidUrl(string url)
        {           
            //Regex to check if string starts with http or https
            Regex rgx = new Regex(@"^(http|https)://.*$");

            //Regex to remove the text content
            if(!string.IsNullOrWhiteSpace(url) && rgx.IsMatch(url))
            {
                return true;
            }

            return false;
        }
    }
}
