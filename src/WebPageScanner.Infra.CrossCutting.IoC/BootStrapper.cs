﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Conventions;
using WebPageScanner.Application.Interfaces;
using WebPageScanner.Application.Services;
using WebPageScanner.Domain.Configurations;
using WebPageScanner.Domain.Interfaces.Repository;
using WebPageScanner.Domain.Interfaces.Service;
using WebPageScanner.Domain.Services;
using WebPageScanner.Infra.Data.Mapping;
using WebPageScanner.Infra.Data.Repository;

namespace WebPageScanner.Infra.CrossCutting.IoC
{
    public static class BootStrapper
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            //Applicaiton
            services.AddTransient<IScannerAppService, ScannerAppService>();

            //Domain
            services.AddTransient<IScannerService, ScannerService>();
            services.Configure<DbSettingsOptions>(configuration.GetSection("DbSettings"));

            //Data
            services.AddTransient<IScannerRepository, ScannerRepository>();

            var pack = new ConventionPack
            {
                new CamelCaseElementNameConvention(),
                new IgnoreExtraElementsConvention(true)
            };

            ConventionRegistry.Register("Web Scanner Conventions", pack, t => true);
            MappingProfile.Map();

        }
    }
}
