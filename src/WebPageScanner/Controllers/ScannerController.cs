﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using WebPageScanner.Application.Interfaces;
using WebPageScanner.Application.ViewModels;
using WebPageScanner.Domain.Mappers;
using validation = WebPageScanner.Domain.Validations.ValidationScannerController;

namespace WebPageScanner.Services.Api.Controllers
{
    [ApiController]
    [Route("api/")]
    public class ScannerController : ControllerBase
    {
        private readonly IScannerAppService _scannerAppService;

        public ScannerController(IScannerAppService scannerAppService)
        {
            _scannerAppService = scannerAppService;
        }

        /// <summary>
        /// Method to return all images and top 10 words from website
        /// </summary>
        /// <param name="url"> url from website with http:// or https://</param>
        /// <returns></returns>
        [Route("pagescanner/get")]
        [HttpGet]
        [ProducesResponseType(typeof(PageResponseViewModel), StatusCodes.Status200OK)]        
        [ProducesResponseType(typeof(ErrorViewModel), StatusCodes.Status400BadRequest)]        
        public ActionResult<PageResponseViewModel> Get([FromQuery] string url)
        {
            try
            {               
                if (validation.isValidUrl(url))
                {
                    var wpData =  _scannerAppService.GetWebPageData(url);
                    var response = PageResponseViewModelMapper.Map(wpData);
                    return Ok(response);
                }
                else
                {
                    return BadRequest(new ErrorViewModel("Invalid url, should be used http:// or https://"));
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorViewModel(ex.Message));
            }            
        }
    }
}
