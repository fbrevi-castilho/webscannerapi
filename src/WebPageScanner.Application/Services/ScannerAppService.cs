﻿using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using WebPageScanner.Application.Interfaces;
using WebPageScanner.Domain.Interfaces.Repository;
using WebPageScanner.Domain.Interfaces.Service;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Application.Services
{
    public class ScannerAppService : IScannerAppService
    {

        private readonly ILogger<ScannerAppService> _logger;
        private readonly IScannerRepository _scannerRepository;
        private readonly IScannerService _scannerService;
        const int minutes = 10;
        public ScannerAppService(ILogger<ScannerAppService> logger,
            IScannerRepository scannerRepository, IScannerService scannerService)
        {
            _logger = logger;
            _scannerRepository = scannerRepository;
            _scannerService = scannerService;
        }


        public WebPageDataHistory GetWebPageData(string url)
        {
            try
            {
                //verify if already exists to last X minutes
                var webPageData = _scannerRepository.GetPageData(url, minutes);
                if (webPageData.Any())
                {
                   return webPageData.OrderBy(x => x.Created).Last();
                }
                else {                     
                   return _scannerService.GetWebPageData(url);
                }
            }            
            catch (Exception ex)
            {
                _logger.LogError($"Error to get webpage data. Message {ex.Message}");
                throw;
            }

        }
    }
}
