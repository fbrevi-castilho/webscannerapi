﻿using System.Threading.Tasks;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Application.Interfaces
{
    public interface IScannerAppService
    {
        public WebPageDataHistory GetWebPageData(string url);

    }
}
