﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Application.ViewModels
{
    public class PageResponseViewModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("images")]
        public List<string> Images { get; set; }

        [JsonProperty("words")]
        public List<Word> Words { get; set; }
    }
}
