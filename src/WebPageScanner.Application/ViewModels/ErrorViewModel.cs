﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPageScanner.Application.ViewModels
{
    public class ErrorViewModel
    {
        public ErrorViewModel(string reason)
        {
            Error = reason;
        }

        public string Error { get; private set; }
    }
}
