﻿using WebPageScanner.Application.ViewModels;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Domain.Mappers
{
    public static class PageResponseViewModelMapper
    {
        public static PageResponseViewModel Map(WebPageDataHistory webPageDataHistory)
        {
            return new PageResponseViewModel()
            {
                Images = webPageDataHistory?.Images,
                Url = webPageDataHistory?.Url,
                Words = webPageDataHistory?.Words
            };
        }
    }
}
