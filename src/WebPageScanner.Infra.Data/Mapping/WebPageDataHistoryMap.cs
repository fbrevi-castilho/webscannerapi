﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Infra.Data.Mapping
{
    public static class WebPageDataHistoryMap
    {
        public static void Map()
        {
           BsonClassMap.RegisterClassMap<WebPageDataHistory>(cm =>
               {
                   cm.AutoMap();
                   cm.MapIdMember(c => c.Id).SetIgnoreIfDefault(true).SetSerializer(new StringSerializer(BsonType.ObjectId));
               }
           );
        }
    }
}
