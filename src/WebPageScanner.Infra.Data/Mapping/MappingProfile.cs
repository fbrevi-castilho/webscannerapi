﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;

namespace WebPageScanner.Infra.Data.Mapping
{
    public static class MappingProfile
    {
        public static void Map()
        {
            BsonSerializer.RegisterSerializer(typeof(DateTime), new DateTimeSerializer(DateTimeKind.Local));
            WebPageDataHistoryMap.Map();
        }
    }
}
