﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebPageScanner.Domain.Configurations;

namespace WebPageScanner.Infra.Data.Repository
{
    public class MongoDBRepository
    {

        private IMongoClient _client;
        private IMongoDatabase _databse;
        private readonly DbSettingsOptions _dbSettings;
        public MongoDBRepository(IOptions<DbSettingsOptions> dbSettings)
        {
            _dbSettings = dbSettings.Value;
            _client = new MongoClient(_dbSettings.MongoConnection);
            _databse = _client.GetDatabase(_dbSettings.DataBase);
        }

        public IMongoDatabase DataBase
        {
            get
            {
                return _databse;
            }
        }
    }
}
