﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebPageScanner.Domain.Configurations;
using WebPageScanner.Domain.Interfaces.Repository;
using WebPageScanner.Domain.Models;

namespace WebPageScanner.Infra.Data.Repository
{
    public class ScannerRepository : MongoDBRepository, IScannerRepository
    {
        public ScannerRepository(IOptions<DbSettingsOptions> dbSettings) : base(dbSettings) { }

        public List<WebPageDataHistory> GetPageData(string url, int minutes)
        {
            DateTime dt = DateTime.UtcNow.AddMinutes(-minutes);
            return  DataBase.GetCollection<WebPageDataHistory>("webPageDataHistory")
                   .Find(x => x.Url == url && x.Created >= dt)
                   .ToList();            
        }

        public void InsertOne(WebPageDataHistory webPageDataHistory)
        {
            DataBase.GetCollection<WebPageDataHistory>("webPageDataHistory").InsertOne(webPageDataHistory);
        }
    }
}
