﻿using FizzWare.NBuilder;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using WebPageScanner.Application.Services;
using WebPageScanner.Domain.Interfaces.Repository;
using WebPageScanner.Domain.Interfaces.Service;
using WebPageScanner.Domain.Models;
using Xunit;

namespace WebPageScanner.Tests.Application
{        
    public class ScannerAppServiceTest
    {
        #region Attributes
        private readonly ScannerAppService _scannerAppService;
        private readonly Mock<ILogger<ScannerAppService>> _logger;
        private readonly Mock<IScannerRepository> _scannerRepository;
        private readonly Mock<IScannerService> _scannerService;
        #endregion

        #region Initialize
        public ScannerAppServiceTest()
        {
            _logger = new Mock<ILogger<ScannerAppService>>();
            _scannerService = new Mock<IScannerService>();
            _scannerRepository = new Mock<IScannerRepository>();
            _scannerAppService = new ScannerAppService(_logger.Object, _scannerRepository.Object, _scannerService.Object);
        }
        #endregion

        #region Test Methods
        [Fact]
        public void ScannerAppService_GetWebPageData_SaveAndReturn()
        {
            //Arrange            
            var url = "https://www.example.com";
            var webPageDataHistory = Builder<WebPageDataHistory>.CreateNew().Build();
            _scannerRepository.Setup(s => s.GetPageData(It.IsAny<string>(), It.IsAny<int>()))
                              .Returns(new List<WebPageDataHistory>());

            _scannerService.Setup(s => s.GetWebPageData(It.IsAny<string>())).Returns(webPageDataHistory);

            _scannerRepository.Setup(s => s.GetPageData(It.IsAny<string>(), It.IsAny<int>()))
                              .Returns(new List<WebPageDataHistory>());                                        

            //Act
             _scannerAppService.GetWebPageData(url);

            //Assert
            _scannerRepository.Verify(x => x.GetPageData(It.IsAny<string>(), It.IsAny<int>()), Times.Once);
            _scannerService.Verify(x => x.GetWebPageData(It.IsAny<string>()), Times.Once);           
        }


        [Fact]
        public void ScannerAppService_GetWebPageData_GetDataBaseAndReturn()
        {
            //Arrange            
            var url = "https://www.example.com";

            List<WebPageDataHistory> fakeWebPageDataHistory = new List<WebPageDataHistory> {
                 Builder<WebPageDataHistory>.CreateNew().Build(),
                 Builder<WebPageDataHistory>.CreateNew().Build()          
            };           

            _scannerService.Setup(s => s.GetWebPageData(It.IsAny<string>())).Returns(new WebPageDataHistory());

            _scannerRepository.Setup(s => s.GetPageData(It.IsAny<string>(), It.IsAny<int>()))
                              .Returns(fakeWebPageDataHistory);

            _scannerRepository.Setup(s => s.InsertOne(It.IsAny<WebPageDataHistory>()));


            //Act
            _scannerAppService.GetWebPageData(url);

            //Assert
            _scannerRepository.Verify(x => x.GetPageData(It.IsAny<string>(), It.IsAny<int>()), Times.Once);
            _scannerService.Verify(x => x.GetWebPageData(It.IsAny<string>()), Times.Never);
            _scannerRepository.Verify(x => x.InsertOne(It.IsAny<WebPageDataHistory>()), Times.Never);
        }


        [Fact]
        public void ScannerAppService_GetWebPageData_ThrowException()
        {
            //Arrange            
            var url = "www.example.com";


            _scannerService.Setup(s => s.GetWebPageData(It.IsAny<string>())).Throws(new Exception());

            _scannerRepository.Setup(s => s.GetPageData(It.IsAny<string>(), It.IsAny<int>()))
                              .Returns(new List<WebPageDataHistory>());

            _scannerRepository.Setup(s => s.InsertOne(It.IsAny<WebPageDataHistory>()));

            //Act & Assert
            Assert.Throws<Exception>(() => _scannerAppService.GetWebPageData(url));
            _scannerRepository.Verify(x => x.GetPageData(It.IsAny<string>(), It.IsAny<int>()), Times.Once);
            _scannerService.Verify(x => x.GetWebPageData(It.IsAny<string>()), Times.Once);
            _scannerRepository.Verify(x => x.InsertOne(It.IsAny<WebPageDataHistory>()), Times.Never);
        }
        #endregion
    }
}
