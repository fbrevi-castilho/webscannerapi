﻿using WebPageScanner.Domain.Validations;
using Xunit;

namespace WebPageScanner.Tests.Domain
{
    public class ValidationScannerControllerTest
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("    ")]
        [InlineData("123")]
        [InlineData("www.teste.com")]
        [InlineData("http:/www.teste.com")]
        [InlineData("https:/www.teste.com")]
        [InlineData("htps://www.teste.com")]
        [InlineData("htp://www.teste.com")]
        public void IsValidUrl_InvalidParameter_False(string url)
        {
            var isValid = ValidationScannerController.isValidUrl(url);
            Assert.False(isValid);
        }

        [Theory]        
        [InlineData("http://www.teste.br")]
        [InlineData("http://www.teste.uk")]
        [InlineData("https://www.teste.com")]
        [InlineData("http://www.teste.com")]
        public void IsValidUrl_ValidParameter_True(string url)
        {
            var isValid = ValidationScannerController.isValidUrl(url);
            Assert.True(isValid);
        }
    }
}
